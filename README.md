# A Symfony boiler plate for docker
---
This is a modified version of the [docker-symfony](https://github.com/eko/docker-symfony) repo. Ships with the following services:

* PHP-fpm
* Nginx
* MySQL
* PhpMyAdmin
* Elk - logging

## Installation
---

Create a project directory
```
mkdir my-app && cd my-app
```

Using Composer, create a  new app
```
composer create-project symfony/website-skeleton app
```

Clone the repo into a separate folder
```
git clone git@bitbucket.org:michaelps/docker-symfony-boilerplate.git docker
```

Add an entry to your hosts file:  
**Linux:** `/etc/hosts`  
**Windows:** `c:\Windows\System32\Drivers\etc\hosts`
```
symfony.localhost 127.0.0.1
```

Cd into your docker directory and run the project
```
docker-compose up -d
```

### Connecting to Mysql server
For creating a connection between the Symfony-app and the MySQL-server, you'll have to define an URL int he `.env` file. For this setup, something like this will most likely be correct.

```
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name
```

For more info, check out the official docs on [databases and the Doctrine ORM](https://symfony.com/doc/current/doctrine.html)

## URL's to services
---
* **App (Symfony):** [symfony.localhost](http://symfony.localhost:80)
* **Logger (Elk):** [symfony.localhost:81](http://symfony.localhost:81)
* **PhpMyAdmin (DB):** [symfony.localhost:82](http://symfony.localhost:82)